package service;

import repository.AccountRepository;
import domain.Account;

public class TransferService {

	AccountRepository repository;

	public TransferService(AccountRepository repository) {
		this.repository = repository;
	}

	public String transfer(String sourceNumber, String targetNumber, double amount) {
		Account sourceAccount = repository.findByNumber(sourceNumber);
		Account targetAccount = repository.findByNumber(targetNumber);
		if (sourceAccount == null || targetAccount == null) {
			return "Couldn't find accounts";
		}
		if (sourceAccount.getBalance() >= amount) {
			sourceAccount.setBalance(sourceAccount.getBalance() - amount);
			targetAccount.setBalance(targetAccount.getBalance() + amount);
			repository.save(sourceAccount);
			repository.save(targetAccount);
			return "Tranfer...[OK]";
		}
		return "No enough balance to tranfer";
	}
}
