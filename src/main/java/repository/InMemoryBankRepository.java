package repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import domain.Account;
import domain.Bank;

public class InMemoryBankRepository implements BankRepository{

	Map<String, Bank> banks;

	public InMemoryBankRepository() {
		banks = new HashMap<String, Bank>();
		Bank b1 = new Bank("1001", "BSP");
		Bank b2 = new Bank("1002", "VVBA");
		
		this.save(b1);
		this.save(b2);
		
	}

	
	public Bank findByCode(String code) {
		return Bank.copy(banks.get(code));
	}

	
	public List<Bank> findAll() {
		List<Bank> list = new ArrayList<Bank>();
		for (Bank a : banks.values()) {
			list.add(Bank.copy(a));
		}
		return list;
	}


	public Bank save(Bank bank) {
		if (bank.getCode() == null)  {
			bank.setCode("" + (int)(100000 + 1000000*Math.random()));
		}
		banks.put(bank.getCode(), bank);
		return Bank.copy(bank);
	}

	public Bank remove(Bank bank) {
		if (banks.containsKey(bank.getCode()))
			return Bank.copy(banks.remove(bank.getCode()));
		return null;
	}
}
