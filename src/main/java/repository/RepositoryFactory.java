package repository;

public interface RepositoryFactory {
	AccountRepository createAccountRepository();
	BankRepository createBankRepository();
	PersonRepository createPersonRepository();
}
