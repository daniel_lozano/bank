package repository;

import java.util.List;

import domain.Account;

public class InMemoryRepositoryFactory implements RepositoryFactory {

	
	public AccountRepository createAccountRepository() {
		return new InMemoryAccountRepository();
	}

	
	public PersonRepository createPersonRepository() {
		return null;
	}


	public BankRepository createBankRepository() {
		return new InMemoryBankRepository();
	}

}
