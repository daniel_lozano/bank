package repository;

import java.util.List;

import domain.Bank;

public interface BankRepository {
	Bank findByCode(String number);

	List<Bank> findAll();

	Bank save(Bank Bank);

	Bank remove(Bank Bank);
}
