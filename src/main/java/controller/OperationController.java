package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import repository.AccountRepository;
import repository.InMemoryAccountRepository;
import service.TransferService;

@Controller
public class OperationController {
	@RequestMapping("/transference")
	String transference_submit() {
		return "operation_transference";
	}

	@RequestMapping("/transference_submit")
	String transference_submit(ModelMap model, @RequestParam String source,
			@RequestParam String target, @RequestParam Integer amount) {

		if (source == null || target == null || amount == null) {
			model.addAttribute("result", "Empty fields, please fill all.");
			return "operation_result";
		}

		AccountRepository varAccounts = new InMemoryAccountRepository();

		TransferService service = new TransferService(varAccounts);
		model.addAttribute("result", service.transfer(source, target, amount));
		return "operation_result";
	}
}
