package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import repository.AccountRepository;
import repository.InMemoryAccountRepository;

@Controller
public class AccountController {
	
	@RequestMapping("/accounts_list")
	String accounts_list(ModelMap model){
		AccountRepository varAccounts = new InMemoryAccountRepository();		
		
		model.addAttribute("accounts", varAccounts.findAll());
		return "accounts_list";
	}
}
