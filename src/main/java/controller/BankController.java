package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import repository.BankRepository;
import repository.InMemoryBankRepository;

@Controller
public class BankController {
	
	@RequestMapping("/banks_list")
	String banks_list(ModelMap model){
		BankRepository varBanks = new InMemoryBankRepository();		
		
		model.addAttribute("banks", varBanks.findAll());
		return "banks_list";
	}
}
