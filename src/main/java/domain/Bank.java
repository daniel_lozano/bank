package domain;

public class Bank {
	private String code;
	private String name;	
	
	
	public Bank() {
	}

	public Bank(String code, String name) {
		this.code = code;
		this.name = name;
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public static Bank copy(Bank bank) {
		if (bank == null) {
			return null;
		}
		Bank copy = new Bank();
		copy.setCode(bank.getCode());
		copy.setName(bank.getName());
		return copy;
	}
	
	@Override
	public String toString() {
		return "{code: " + code + ", name: " + name + "}";
	}
}
